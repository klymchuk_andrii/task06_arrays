package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class View {
    private Controller controller;
    private static Logger logger;

    public View(){
        controller = new Controller();
        logger = LogManager.getLogger(View.class);
    }

    public void show(){

        controller.createArrayWithDataOfArrays();
        logger.trace(controller.getAllArrays()+"\n");

        controller.deleteConsecutiveSameElements();
        logger.trace(controller.getAllArrays()+"\n");

        controller.clearArray();
        controller.createArrayWithDataOfOneArray();
        logger.trace(controller.getAllArrays()+"\n");

        controller.deleteRepetitiveNumbers();
        logger.trace(controller.getAllArrays()+"\n");

        logger.trace(controller.getInformationAboutDoor());

        logger.trace("Hero can death : "+controller.getCountOfDeath()+" time");

        logger.trace("Way to survive: " + controller.findWayForLife());

        logger.trace("Dequeue and priorityQueue \n" + controller.checkQueue());
    }
}
