package com.klymchuk.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MyPriorityQueue<E> {
    List<E> list;

    public MyPriorityQueue() {
        this.list = new ArrayList<>();
    }

    public boolean add(E element) {
        list.add(element);
        list = list.stream().sorted().collect(Collectors.toList());
        return true;
    }


    public E peek() {
        E element = list.get(list.size() - 1);
        return element;
    }

    public E poll() {
        if(list.isEmpty()){
            return null;
        }
        E element = list.get(list.size() - 1);
        list.remove(list.size() - 1);
        return element;
    }

    public int size() {
        return list.size();
    }

    @Override
    public String toString() {
        String rezult="[ ";

        for (E element:list) {
            rezult +=element+", ";
        }
        rezult +="]";
        return rezult;
    }
}
