package com.klymchuk.model.game;

public class Artifact implements Magic{
    private int power;

    public Artifact(int powerParam) {
        power = powerParam;
    }

    @Override
    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Artifact power:" + power + "\n";
    }
}
