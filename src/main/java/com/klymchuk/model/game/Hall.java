package com.klymchuk.model.game;

public class Hall {
    private Door door;
    private Hero hero;
    private int countOfDeath;
    private String sequences;

    public Hall() {
        door = new Door();
        door.generateMonsterAndArtifact();
        hero = new Hero();
        countOfDeath = 0;
        sequences ="";
    }

    public String getInformationAboutDoor() {
        return door.toString();
    }

    public int numberOfDeath(int number) {
        if (hero.getPower() + door.getDoor10(number).getPower() < 0) {
            countOfDeath++;
        }
        number++;
        if (number != 10) {
            numberOfDeath(number);
        }
        return countOfDeath;
    }

    public String findWayToLife(int number) {
        if(number==10){
            return sequences;
        }
        if (hero.getPower() + door.getDoor10(number).getPower() > 0) {
            hero.setPower(hero.getPower() + door.getDoor10(number).getPower());
            sequences += "door" + (number + 1) + " -> ";
            findWayToLife(++number);
        }
        else{
            findWayToLife(++number);
            sequences += "dor" + (number) + " -> ";
        }

        return sequences;
    }
}
