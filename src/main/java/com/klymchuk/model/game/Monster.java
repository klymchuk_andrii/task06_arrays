package com.klymchuk.model.game;

public class Monster implements Magic {
    private int power;

    public Monster(int powerParam) {
        power = powerParam;
    }

    @Override
    public int getPower() {
        return -power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Monster power:" + power + "\n";
    }
}
