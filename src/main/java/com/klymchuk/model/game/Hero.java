package com.klymchuk.model.game;

public class Hero {
    private int power;

    public Hero() {
        power = 25;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
