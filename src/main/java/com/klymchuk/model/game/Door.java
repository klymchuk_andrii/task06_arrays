package com.klymchuk.model.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Door {
    private List<Magic> door10;

    public Door() {
        door10 = new ArrayList<>();
    }

    public void generateMonsterAndArtifact() {
        for (int i = 0; i < 10; i++) {
            if (new Random().nextInt(100) + 1 > 70) {
                door10.add(new Monster(new Random().nextInt(96) + 5));
            } else {
                door10.add(new Artifact(new Random().nextInt(71) + 10));
            }
        }
    }

    public Magic getDoor10(int i) {
        return door10.get(i);
    }

    @Override
    public String toString() {
        String aboutDoor = "";
        for (int i = 0; i < door10.size(); i++) {
            aboutDoor += "door" + (i+1) + "  " + (door10.get(i)).toString();
        }
        return aboutDoor;
    }
}
