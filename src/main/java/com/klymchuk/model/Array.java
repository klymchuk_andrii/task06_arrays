package com.klymchuk.model;

import java.util.ArrayList;
import java.util.List;

public class Array {
    public List<Integer> array1;
    public List<Integer> array2;
    public List<Integer> array3;

    public Array() {
        array1 = new ArrayList<Integer>();
        array2 = new ArrayList<Integer>();
        array3 = new ArrayList<Integer>();
    }

    public void initializationArrays() {
        for (int i = 0; i < 20; i++) {
            array1.add((int) (Math.random() * 19 + 1));
            array2.add((int) (Math.random() * 19 + 1));
        }
    }

    public void createArrayWithDataOfArrays() {
        for (int i : array1) {
            if (array2.indexOf(i) != -1) {
                array3.add(i);
            }
        }
        for (int i : array2) {
            if (array1.indexOf(i) != -1) {
                array3.add(i);
            }
        }
    }

    public void createArrayWithDataOfOneArray() {
        for (int i : array1) {
            if (array2.indexOf(i) == -1) {
                array3.add(i);
            }
        }
    }

    public void deleteRepetitiveNumbers() {
        int number=0;
        for (int i = 0; i < array3.size(); i++) {
            if (findNumberOfRepetition(array3.get(i)) > 2) {
                number=array3.get(i);
                while(array3.indexOf(number)!=-1){
                    array3.remove(array3.indexOf(number));
                }
            }
        }
    }

    private int findNumberOfRepetition(int number) {
        int count = 0;
        for (int i : array3) {
            if (i == number) {
                count++;
            }
        }
        return count;
    }

    public void deleteConsecutiveSameElements(){
        for(int i=0;i<array3.size()-1;i++){
            if(array3.get(i)==array3.get(i+1)){
                array3.remove(i);
            }
        }
    }

}
