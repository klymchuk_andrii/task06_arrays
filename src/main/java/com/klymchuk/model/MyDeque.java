package com.klymchuk.model;

import java.util.ArrayList;
import java.util.List;

public class MyDeque<E> {
    List<E> list;

    public MyDeque() {
        this.list = new ArrayList<>();
    }

    public void addFirst(E element) {
        list.add(element);
        for (int i = 0; i < list.size() - 1; i++) {
            list.set(list.size() - 1 - i, list.get(list.size() - 2 - i));
        }
        list.set(0, element);
    }

    public E removeFirst() {
        if(list.isEmpty()){
            return null;
        }
        E removeElement = list.get(0);
        list.remove(0);
        return removeElement;
    }

    public E getFirst() {
        return list.get(0);
    }

    public void addLast(E element) {
        list.add(element);
    }

    public E removeLast() {
        if(list.isEmpty()){
            return null;
        }
        E removeElement = list.get(list.size() - 1);
        list.remove(list.size() - 1);
        return removeElement;
    }

    public E getLast(){
        return list.get(list.size() - 1);
    }

    @Override
    public String toString() {
        String rezult="[ ";

        for (E element:list) {
            rezult +=element+", ";
        }
        rezult +="]";
        return rezult;
    }
}
