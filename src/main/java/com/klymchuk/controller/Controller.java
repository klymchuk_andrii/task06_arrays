package com.klymchuk.controller;

import com.klymchuk.model.Array;
import com.klymchuk.model.MyDeque;
import com.klymchuk.model.MyPriorityQueue;
import com.klymchuk.model.game.Hall;

public class Controller {
    private Array array;
    private Hall hall;
    private MyDeque<Integer> deque;
    private MyPriorityQueue<Integer> priorityQueue;

    public Controller() {
        hall = new Hall();
        array = new Array();
        array.initializationArrays();
        deque = new MyDeque<>();
        priorityQueue = new MyPriorityQueue();
    }

    public String getAllArrays() {
        return array.array1.toString() + "\n" + array.array2.toString()
                + "\n" + array.array3.toString();
    }

    public void createArrayWithDataOfArrays() {
        array.createArrayWithDataOfArrays();
    }

    public void deleteConsecutiveSameElements() {
        array.deleteConsecutiveSameElements();
    }

    public void createArrayWithDataOfOneArray() {
        array.createArrayWithDataOfOneArray();
    }

    public void deleteRepetitiveNumbers() {
        array.deleteRepetitiveNumbers();
    }

    public void clearArray() {
        array.array3.clear();
    }

    public String getInformationAboutDoor() {
        return hall.getInformationAboutDoor();
    }

    public int getCountOfDeath() {
        return hall.numberOfDeath(0);
    }

    public String findWayForLife() {
        String sequences;
        sequences = hall.findWayToLife(0);
        if (sequences.equals("")) {
            return "has no way to survive";
        }
        return sequences;
    }

    public String checkQueue() {
        String rezult;
        deque.addFirst(1);
        deque.addFirst(2);
        deque.addLast(3);
        if (deque.getFirst().equals(2)) {
            deque.removeFirst();
        }
        rezult = deque.toString() + "\n";


        priorityQueue.add(2);
        priorityQueue.add(1);

        if (priorityQueue.size() == 2) {
            priorityQueue.poll();
        }

        rezult += priorityQueue.toString() + "\n";

        return rezult;
    }

}
